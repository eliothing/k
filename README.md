![](https://elioway.gitlab.io/elioolympians/k/elio-k-logo.png)

> It's ok, really, fine, 99 maybe 97% fine, **the elioWay**

# k

**k** is short for "Ok", short for "Okay", short for "Yes, it's all fine.", short for "Yes, it's 99%, maybe 97% fine."

This will be the basis of code deciding whether a `Thing`s field, `Thing` or a list of `Thing`s can report "k".

Starter pack for an eliothing app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [k Documentation](https://elioway.gitlab.io/elioolympians/k/)

## Installing

- [Installing k](https://elioway.gitlab.io/elioolympians/k/installing.html)

## Requirements

- [elioolympians Prerequisites](https://elioway.gitlab.io/elioolympians/installing.html)

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Seeing is Believing

```
You're seeing it.
```

- [elioolympians Quickstart](https://elioway.gitlab.io/elioolympians/quickstart.html)
- [k Quickstart](https://elioway.gitlab.io/elioolympians/k/quickstart.html)

# Credits

- [k Credits](https://elioway.gitlab.io/elioolympians/k/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioolympians/k/apple-touch-icon.png)
