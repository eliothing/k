const should = require("chai").should()
const k = require("../k")

describe("module | k", function(hooks) {
  it("fetches k", () => {
    k().should.equal("k")
  })
})
