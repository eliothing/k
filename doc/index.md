<aside>
  <dl>
  <dd>Father, who art judge</dd>
  <dd>Of all things made, and judgest only right.</dd>
  <dd>Or shall the Adversary thus obtain</dd>
  <dd>His end, and frustrate thine?</dd>
</dl>
</aside>

**k** will be **the elioWay**s validation platform. It should not be difficult to write, given that Schema comes with all the meta information you would need.

The elioWay is not to prevent users from submitting changes. There is only one required field: `identifier`. A "thing" must have an `identifier` or it can't exist. If it can exist, it can be stored. And it if can be stored, it can be edited later.

**k** doesn't insist users complete the form - it will be used by _endpoints_ to establish if a record is "complete" or "whole" or some other check - and **k** output can be used to report which areas of the record or records in the **list** need more data input before "passing **k**s inspection".

A **k** check might be used as the first step in a `publishT` _endpoint_ wizard.
